﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyFinal5QMTWDM.Entities
{
    /*
      ,[idProfesor]
      ,[descripcion]
      ,[fechaCreado]
    */
    public class Grupo
    {
        public int Id { get; set; }
        public int IdProfesor { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaCreado { get; set; }
    }
}
