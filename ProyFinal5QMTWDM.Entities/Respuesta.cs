﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyFinal5QMTWDM.Entities
{
    public class RespuestaMin
    {
        public string Mensaje { get; set; }
        public bool Respuesta { get; set; }
        public string Error { get; set; }
    }

    public class RespuestaUsuario : RespuestaMin
    {
        public bool Existe { get; set; }
        public Usuario Usuario { get; set; }
    }
}
