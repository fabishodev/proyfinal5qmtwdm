﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyFinal5QMTWDM.Entities
{
    /* 
      ,[idAlumno]
      ,[idGrupo]
      ,[calificacion]
      ,[fechaCreado]
     */
    public class Alumno
    {
        public int Id { get; set; }
        public int IdAlumno { get; set; }
        public int IdGrupo { get; set; }
        public decimal Calificacion { get; set; }
        public DateTime FechaCreado { get; set; }
    }

    public class AlumnoUsuario: Alumno
    {
        public string NombreCompleto { get; set; }
    }

    public class AlumnoUsuarioGrupo : AlumnoUsuario
    {      
        public string NombreGrupo { get; set; }
    }

    public class AlumnoUsuarioGrupoProfesor : AlumnoUsuarioGrupo
    {
        public int IdProfesor { get; set; }
        public string NombreProfesor { get; set; }
    }

    public class AlumnoGrupoCalificaciones 
    {
        public int Id { get; set; }
        public string NombreProfesor { get; set; }
        public string NombreGrupo { get; set; }
        public decimal Calificacion { get; set; }
    }
}
