﻿using System;

namespace ProyFinal5QMTWDM.Entities
{
    public class UsuarioMin
    {
        public string Correo { get; set; }
        public string Password { get; set; }
    }
    public class Usuario : UsuarioMin
    {
        public int Id { get; set; }
        public string NombreCompleto { get; set; }
        public int Tipo { get; set; }
        public DateTime Fecha { get; set; }
    }
}
