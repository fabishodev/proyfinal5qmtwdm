﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProyFinal5QMTWDM.Api.Services.Interfaces;
using ProyFinal5QMTWDM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyFinal5QMTWDM.Api.Controllers
{

    [ApiController]
    public class UsuarioController : ControllerBase
    {

        private readonly IUsuarioService _usuarioService;

        public UsuarioController(IUsuarioService usuarioService)
        {
            _usuarioService = usuarioService;
        }

        [HttpGet]
        [Route("api/[controller]/lista")]
        public ActionResult<List<Usuario>> ObtenerUsuarios()
        {
            var respuesta = new List<Usuario>();
            respuesta = _usuarioService.ObtenerUsuarios();
            return Ok(respuesta);
        }

        [HttpPost]
        [Route("api/[controller]/login")]
        public ActionResult<RespuestaUsuario> Obtener([FromBody] UsuarioMin usuarioMin)
        {
            if (usuarioMin == null)
                return BadRequest();

            var usuario = _usuarioService.Obtener(usuarioMin);
            var respuesta = new RespuestaUsuario();

            if (usuario != null)
            {
                respuesta.Mensaje = "Bienvenido " + usuario.NombreCompleto;
                respuesta.Respuesta = true;
                respuesta.Existe = true;
                respuesta.Usuario = usuario;
            }
            else
            {
                respuesta.Mensaje = "No ha sido regsitrado por el Administrador del Sistema";
                respuesta.Respuesta = true;
                respuesta.Existe = false;
                respuesta.Usuario = null;
            }

            return Ok(respuesta);
        }

        [HttpPost]
        [Route("api/[controller]/crear")]
        public ActionResult<RespuestaMin> Crear([FromBody] Usuario usuario)
        {
            if (usuario == null)
                return BadRequest();

            var response = _usuarioService.Insertar(usuario);
            return Ok(response);
        }
    }
}
