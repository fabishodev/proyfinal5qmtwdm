﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProyFinal5QMTWDM.Api.Services.Interfaces;
using ProyFinal5QMTWDM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyFinal5QMTWDM.Api.Controllers
{  
    [ApiController]
    public class AlumnoController : ControllerBase
    {
        private readonly IAlumnoService _alumnoService;

        public AlumnoController(IAlumnoService alumnoService)
        {
            _alumnoService = alumnoService;
        }

       

        [HttpGet]
        [Route("api/[controller]/lista/{idGrupo}")]
        public ActionResult<List<AlumnoUsuarioGrupoProfesor>> ObtenerAlumnosUsuarioGrupo(int idGrupo)
        {
            var respuesta = new List<AlumnoUsuarioGrupoProfesor>();           
            respuesta = _alumnoService.ListaAlumnoUsuarioGrupo(idGrupo);
            return Ok(respuesta);
        }

        [HttpPost]
        [Route("api/[controller]/crear")]
        public ActionResult<RespuestaMin> Crear([FromBody] Alumno alumno)
        {
            if (alumno == null)
                return BadRequest();

            var response = _alumnoService.Insertar(alumno);
            return Ok(response);
        }

        [HttpPost]
        [Route("api/[controller]/calificacion/actualizar")]
        public ActionResult<RespuestaMin> Actualizar([FromBody] Alumno alumno)
        {
            if (alumno == null)
                return BadRequest();

            var response = _alumnoService.Actualizar(alumno);
            return Ok(response);
        }

        [HttpGet]
        [Route("api/[controller]/lista/calificaciones/{idAlumno}")]
        public ActionResult<List<AlumnoGrupoCalificaciones>> ObtenerAlumnosCalificaciones(int idAlumno)
        {
            var respuesta = new List<AlumnoGrupoCalificaciones>();
            respuesta = _alumnoService.ListaAlumnoGrupoCalificaciones(idAlumno);
            return Ok(respuesta);
        }
    }
}
