﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProyFinal5QMTWDM.Api.Services.Interfaces;
using ProyFinal5QMTWDM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyFinal5QMTWDM.Api.Controllers
{  
    [ApiController]
    public class GrupoController : ControllerBase
    {
        private readonly IGrupoService _grupoService;

        public GrupoController(IGrupoService grupoService)
        {
            _grupoService = grupoService;
        }

        [HttpGet]
        [Route("api/[controller]/obtener/{idProfesor}")]
        public ActionResult<List<Grupo>> ObtenerGrupos(int idProfesor)
        {
            var respuesta = new List<Grupo>();
            respuesta = _grupoService.ListaGruposProfesor(idProfesor);
            return Ok(respuesta);
        }

        [HttpPost]
        [Route("api/[controller]/crear")]
        public ActionResult<RespuestaMin> Crear([FromBody] Grupo grupo)
        {
            if (grupo == null)
                return BadRequest();

            var response = _grupoService.Insertar(grupo);
            return Ok(response);
        }
    }
}
