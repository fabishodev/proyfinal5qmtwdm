﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace ProyFinal5QMTWDM.Api.DataAccess.Interfaces
{
    public interface IData
    {
        DbConnection DbConnection { get; }
    }

}
