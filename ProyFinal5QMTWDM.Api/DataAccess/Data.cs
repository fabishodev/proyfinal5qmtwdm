﻿using Microsoft.Extensions.Configuration;
using ProyFinal5QMTWDM.Api.DataAccess.Interfaces;
using System.Data.Common;
using System.Data.SqlClient;

namespace ProyFinal5QMTWDM.Api.DataAccess
{
    public class Data : IData
    {
        private readonly IConfiguration _config;

        private string _connectionString = "DatabaseConnectionAzure";
        private SqlConnection _conn;

        public Data(IConfiguration config)
        {
            _config = config;
        }

        public DbConnection DbConnection
        {
            get
            {
                if (_conn == null)
                {
                    _conn = new SqlConnection(_config.GetConnectionString(_connectionString));
                }

                return _conn;
            }
        }
    }
}
