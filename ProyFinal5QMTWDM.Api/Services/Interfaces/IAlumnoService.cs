﻿using ProyFinal5QMTWDM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyFinal5QMTWDM.Api.Services.Interfaces
{
    public interface IAlumnoService
    {
        List<AlumnoUsuarioGrupoProfesor> ListaAlumnoUsuarioGrupo(int _idGrupo);
        RespuestaMin Insertar(Alumno alumno);
        RespuestaMin Actualizar(Alumno alumno);
        List<AlumnoGrupoCalificaciones> ListaAlumnoGrupoCalificaciones(int _idAlumno);
    }
}
