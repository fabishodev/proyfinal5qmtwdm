﻿using ProyFinal5QMTWDM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyFinal5QMTWDM.Api.Services.Interfaces
{
    public interface IGrupoService
    {
        RespuestaMin Insertar(Grupo grupo);
        List<Grupo> ListaGruposProfesor(int _idProfesor);
    }
}
