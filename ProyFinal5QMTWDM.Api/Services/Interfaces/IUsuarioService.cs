﻿using ProyFinal5QMTWDM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyFinal5QMTWDM.Api.Services.Interfaces
{
    public interface IUsuarioService
    {
        Usuario Obtener(UsuarioMin usuarioMin);
        RespuestaMin Insertar(Usuario usuario);
        List<Usuario> ObtenerUsuarios();
    }
}
