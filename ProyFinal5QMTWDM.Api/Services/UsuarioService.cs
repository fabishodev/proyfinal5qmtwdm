﻿using ProyFinal5QMTWDM.Api.Repository.Interfaces;
using ProyFinal5QMTWDM.Api.Services.Interfaces;
using ProyFinal5QMTWDM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyFinal5QMTWDM.Api.Services
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUsuarioRepository _usuarioRepository;

        public UsuarioService(IUsuarioRepository usuarioRepository)
        {
            _usuarioRepository = usuarioRepository;
        }

        public List<Usuario> ObtenerUsuarios()
        {
            var result = _usuarioRepository.ObtenerUsuarios();
            return result;
        }

        public Usuario Obtener(UsuarioMin usuarioMin)
        {
            var result = _usuarioRepository.Obtener(usuarioMin);
            return result;
        }
        public RespuestaMin Insertar(Usuario usuario)
        {
            var result = _usuarioRepository.Insertar(usuario);
            return result;
        }
    }
}
