﻿using ProyFinal5QMTWDM.Api.Repository.Interfaces;
using ProyFinal5QMTWDM.Api.Services.Interfaces;
using ProyFinal5QMTWDM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyFinal5QMTWDM.Api.Services
{
    public class GrupoService : IGrupoService
    {
        private readonly IGrupoRepository _grupoRepository;

        public GrupoService(IGrupoRepository grupoRepository)
        {
            _grupoRepository = grupoRepository;
        }

        public RespuestaMin Insertar(Grupo grupo)
        {
            var result = _grupoRepository.Insertar(grupo);
            return result;
        }

        public List<Grupo> ListaGruposProfesor(int _idProfesor)
        {
            var result = _grupoRepository.ListaGruposProfesor(_idProfesor);
            return result;
        }

    }
}
