﻿using ProyFinal5QMTWDM.Api.Repository.Interfaces;
using ProyFinal5QMTWDM.Api.Services.Interfaces;
using ProyFinal5QMTWDM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyFinal5QMTWDM.Api.Services
{
    public class AlumnoService : IAlumnoService
    {
        private readonly IAlumnoRepository _alumnoRepository;

        public AlumnoService(IAlumnoRepository alumnoRepository)
        {
            _alumnoRepository = alumnoRepository;
        }

        public List<AlumnoUsuarioGrupoProfesor> ListaAlumnoUsuarioGrupo(int _idGrupo)
        {
            var result = _alumnoRepository.ListaAlumnoUsuarioGrupo(_idGrupo);
            return result;
        }

        public RespuestaMin Insertar(Alumno alumno)
        {
            var result = _alumnoRepository.Insertar(alumno);
            return result;
        }

        public RespuestaMin Actualizar(Alumno alumno)
        {
            var result = _alumnoRepository.Actualizar(alumno);
            return result;
        }

        public List<AlumnoGrupoCalificaciones> ListaAlumnoGrupoCalificaciones(int _idAlumno)
        {
            var result = _alumnoRepository.ListaAlumnoGrupoCalificaciones(_idAlumno);
            return result;
        }
    }
}
