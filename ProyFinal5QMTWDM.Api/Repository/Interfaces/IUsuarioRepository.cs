﻿using ProyFinal5QMTWDM.Entities;
using System.Collections.Generic;

namespace ProyFinal5QMTWDM.Api.Repository.Interfaces
{
    public interface IUsuarioRepository
    {
        Usuario Obtener(UsuarioMin usuarioMin);
        RespuestaMin Insertar(Usuario usuario);
        List<Usuario> ObtenerUsuarios();
    }
}
