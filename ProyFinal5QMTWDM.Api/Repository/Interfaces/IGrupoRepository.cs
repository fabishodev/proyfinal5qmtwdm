﻿using ProyFinal5QMTWDM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyFinal5QMTWDM.Api.Repository.Interfaces
{
    public interface IGrupoRepository
    {
        RespuestaMin Insertar(Grupo grupo);
        List<Grupo> ListaGruposProfesor(int _idProfesor);
    }
}
