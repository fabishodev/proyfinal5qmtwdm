﻿using ProyFinal5QMTWDM.Entities;
using System.Collections.Generic;

namespace ProyFinal5QMTWDM.Api.Repository.Interfaces
{
    public interface IAlumnoRepository
    {
        List<AlumnoUsuarioGrupoProfesor> ListaAlumnoUsuarioGrupo(int _idGrupo);
        RespuestaMin Insertar(Alumno alumno);
        RespuestaMin Actualizar(Alumno alumno);
        List<AlumnoGrupoCalificaciones> ListaAlumnoGrupoCalificaciones(int _idAlumno);
    }
}
