﻿using Microsoft.Extensions.Configuration;
using ProyFinal5QMTWDM.Api.DataAccess.Interfaces;
using ProyFinal5QMTWDM.Api.Helpers;
using ProyFinal5QMTWDM.Api.Repository.Interfaces;
using ProyFinal5QMTWDM.Entities;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace ProyFinal5QMTWDM.Api.Repository
{   
    public class GrupoRepository : IGrupoRepository
    {
        private IData _data;
        readonly IConfiguration _configuration;

        public DbConnection DbConnection
        {
            get
            {
                return _data.DbConnection;
            }
        }

        public GrupoRepository(IData data, IConfiguration configuration)
        {
            _data = data;
            _configuration = configuration;
        }


        public List<Grupo> ListaGruposProfesor(int _idProfesor)
        {
            string _error = "";
            Grupos _grupos = new Grupos(_data.DbConnection.ConnectionString);
            List<Grupo> lista = new List<Grupo>();
            lista = _grupos.ObtenerGruposProfesor(_idProfesor, ref _error);
            return lista;
        }

        public RespuestaMin Insertar(Grupo grupo)
        {
            RespuestaMin result = new RespuestaMin();
            try
            {
                Grupos _grupos = new Grupos(_data.DbConnection.ConnectionString);              

                string _error = "";

                if (_grupos.InsertarGrupo(grupo.IdProfesor, grupo.Nombre, ref _error))
                {
                    result.Mensaje = "Grupo insertado correctamente!!!";
                    result.Respuesta = true;
                    result.Error = _error;
                }
                else
                {
                    result.Mensaje = "No se pudo insertar el grupo!!!";
                    result.Respuesta = true;
                    result.Error = _error;
                }

                return result;
            }
            catch (Exception ex)
            {
                result.Mensaje = ex.InnerException.ToString();
                result.Respuesta = false;
                result.Error = ex.ToString();
                return result;
            }
        }
    }
}
