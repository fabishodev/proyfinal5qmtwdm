﻿using Microsoft.Extensions.Configuration;
using ProyFinal5QMTWDM.Api.DataAccess.Interfaces;
using ProyFinal5QMTWDM.Api.Helpers;
using ProyFinal5QMTWDM.Api.Repository.Interfaces;
using ProyFinal5QMTWDM.Entities;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace ProyFinal5QMTWDM.Api.Repository
{
    public class AlumnoRepository : IAlumnoRepository
    {
        private IData _data;
        readonly IConfiguration _configuration;

        public DbConnection DbConnection
        {
            get
            {
                return _data.DbConnection;
            }
        }

        public AlumnoRepository(IData data, IConfiguration configuration)
        {
            _data = data;
            _configuration = configuration;
        }

        public List<AlumnoGrupoCalificaciones> ListaAlumnoGrupoCalificaciones(int _idAlumno)
        {
            string _error = "";
            Alumnos _alumnos = new Alumnos(_data.DbConnection.ConnectionString);
            List<AlumnoGrupoCalificaciones> lista = new List<AlumnoGrupoCalificaciones>();
            lista = _alumnos.ObtenerAlumnosCalificaciones(_idAlumno, ref _error);
            return lista;
        }

        public List<AlumnoUsuarioGrupoProfesor> ListaAlumnoUsuarioGrupo(int _idGrupo)
        {
            string _error = "";
            Alumnos _alumnos = new Alumnos(_data.DbConnection.ConnectionString);
            List<AlumnoUsuarioGrupoProfesor> lista = new List<AlumnoUsuarioGrupoProfesor>();
            lista = _alumnos.ObtenerAlumnosUsuarioGrupo(_idGrupo, ref _error);
            return lista;
        }

        public RespuestaMin Insertar(Alumno alumno)
        {
            RespuestaMin result = new RespuestaMin();
            try
            {
                Alumnos _alumnos = new Alumnos(_data.DbConnection.ConnectionString);                

                string _error = "";

                if (_alumnos.InsertarAlumno(alumno.IdAlumno, alumno.IdGrupo, ref _error))
                {
                    result.Mensaje = "Alumno insertado correctamente!!!";
                    result.Respuesta = true;
                    result.Error = _error;
                }
                else
                {
                    result.Mensaje = "No se pudo insertar el alumno!!!";
                    result.Respuesta = true;
                    result.Error = _error;
                }

                return result;
            }
            catch (Exception ex)
            {
                result.Mensaje = ex.InnerException.ToString();
                result.Respuesta = false;
                result.Error = ex.ToString();
                return result;
            }
        }

        public RespuestaMin Actualizar(Alumno alumno)
        {
            RespuestaMin result = new RespuestaMin();
            try
            {
                Alumnos _alumnos = new Alumnos(_data.DbConnection.ConnectionString);

                string _error = "";

                if (_alumnos.ActualizarCalificacion(alumno.Id, alumno.Calificacion, ref _error))
                {
                    result.Mensaje = "Calificación actualizada correctamente!!!";
                    result.Respuesta = true;
                    result.Error = _error;
                }
                else
                {
                    result.Mensaje = "No se pudo actualizar la calificación!!!";
                    result.Respuesta = true;
                    result.Error = _error;
                }

                return result;
            }
            catch (Exception ex)
            {
                result.Mensaje = ex.InnerException.ToString();
                result.Respuesta = false;
                result.Error = ex.ToString();
                return result;
            }
        }

    }
}
