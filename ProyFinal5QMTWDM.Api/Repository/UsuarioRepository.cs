﻿using Microsoft.Extensions.Configuration;
using ProyFinal5QMTWDM.Api.DataAccess.Interfaces;
using ProyFinal5QMTWDM.Api.Helpers;
using ProyFinal5QMTWDM.Api.Repository.Interfaces;
using ProyFinal5QMTWDM.Entities;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace ProyFinal5QMTWDM.Api.Repository
{
    public class UsuarioRepository : IUsuarioRepository
    {
        private IData _data;
        readonly IConfiguration _configuration;

        public DbConnection DbConnection
        {
            get
            {
                return _data.DbConnection;
            }
        }

        public UsuarioRepository(IData data, IConfiguration configuration)
        {
            _data = data;
            _configuration = configuration;
        }

        public List<Usuario> ObtenerUsuarios()
        {
            List<Usuario> lista = new List<Usuario>();
            string err = "";
            Usuarios _usuarios = new Usuarios(_data.DbConnection.ConnectionString);
            string pass = "";
            //pass = Functions.GetSHA256(usuarioMin.Password);
            lista = _usuarios.ObtenerUsuarios(ref err);
            return lista;
        }

        public Usuario Obtener(UsuarioMin usuarioMin)
        {
            Usuario usuario = new Usuario();
            string err = "";
            Usuarios _usuarios = new Usuarios(_data.DbConnection.ConnectionString);
            string pass = "";
            //pass = Functions.GetSHA256(usuarioMin.Password);
            usuario = _usuarios.ObtenerUsuario(usuarioMin.Correo, pass, ref err);
            return usuario;
        }

        public RespuestaMin Insertar(Usuario usuario)
        {
            RespuestaMin result = new RespuestaMin();
            try
            {
                Usuarios _usuarios = new Usuarios(_data.DbConnection.ConnectionString);
                string pass = "";
                pass = Functions.GetSHA256("12345");

                string _error = "";

                if (_usuarios.InsertarUsuario(usuario.Correo, pass, usuario.NombreCompleto, usuario.Tipo.ToString(), ref _error))
                {
                    result.Mensaje = "Usuario insertado correctamente!!!";
                    result.Respuesta = true;
                    result.Error = _error;
                }
                else
                {
                    result.Mensaje = "No se pudo insertar el usuario!!!";
                    result.Respuesta = true;
                    result.Error = _error;
                }

                return result;
            }
            catch (Exception ex)
            {
                result.Mensaje = ex.InnerException.ToString();
                result.Respuesta = false;
                result.Error = ex.ToString();
                return result;
            }
        }
    }
}
