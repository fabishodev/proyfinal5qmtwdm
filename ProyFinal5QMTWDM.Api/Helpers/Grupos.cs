﻿using ProyFinal5QMTWDM.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ProyFinal5QMTWDM.Api.Helpers
{
    public class Grupos
    {
        private readonly string _cadenaConexion = "";

        public Grupos(string cadenaConexion)
        {
            _cadenaConexion = cadenaConexion;
        }

        public List<Grupo> ObtenerGruposProfesor(int _idProfesor, ref string strErr)
        {
            DataTable dtRegistros = new DataTable();
            SqlDataAdapter sqlDataAdapter = null;
            SqlCommand sqlCommand = null;
            SqlConnection sqlConexion = new SqlConnection(_cadenaConexion);
            string sql = null;
            dtRegistros = new DataTable();
            try
            {
                strErr = "";

                sql = "SELECT [id],[idProfesor] ,[nombre] ,[fechaCreado]  FROM [dbo].[Grupos] WHERE [idProfesor] =" + _idProfesor + "";

                sqlCommand = new SqlCommand(sql, sqlConexion);
                sqlCommand.CommandTimeout = 0;
                sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dtRegistros);


                if (dtRegistros.Rows.Count > 0)
                {
                    List<Grupo> lista = new List<Grupo>();
                    for (int i = 0; i < dtRegistros.Rows.Count; i++)
                    {
                        Grupo grupo = new Grupo();
                        grupo.Id = int.Parse(dtRegistros.Rows[i]["id"].ToString());
                        grupo.IdProfesor = int.Parse(dtRegistros.Rows[i]["IdProfesor"].ToString());
                        grupo.Nombre = dtRegistros.Rows[i]["Nombre"].ToString();
                        grupo.FechaCreado = Convert.ToDateTime(dtRegistros.Rows[i]["FechaCreado"].ToString());
                        lista.Add(grupo);
                    }
                    return lista;
                }
                else
                    return null;
            }
            catch (SqlException e)
            {
                sqlConexion.Close();
                strErr = e.Message.ToString() + " " + e.Source.ToString() + " " + e.StackTrace.ToString() + " " + e.TargetSite.Name.ToString();
                return null;
            }
        }

        public bool InsertarGrupo(int _idProfesor, string _nombre, ref string strErr)
        {
            SqlConnection conexion = new SqlConnection(_cadenaConexion);
            conexion.Open();

            SqlTransaction sqlTransaccion = conexion.BeginTransaction(IsolationLevel.ReadCommitted);
            SqlCommand sqlCommand = conexion.CreateCommand();
            sqlCommand.Transaction = sqlTransaccion;

            try
            {
                sqlCommand.CommandText = "INSERT INTO [dbo].[Grupos] ([idProfesor],[nombre],[fechaCreado]) VALUES (" + _idProfesor + ",'" + _nombre.ToUpper().Trim() + "', GETDATE())";
                if (sqlCommand.ExecuteNonQuery() <= 0)
                    return false;
                sqlTransaccion.Commit();
                sqlTransaccion = null;
                conexion.Close();
                return true;
            }
            catch (Exception e)
            {
                if (sqlTransaccion != null)
                {
                    sqlTransaccion.Rollback();
                    sqlTransaccion = null;
                }
                strErr = e.Message.ToString() + " " + e.Source.ToString() + " " + e.StackTrace.ToString() + " " + e.TargetSite.Name.ToString();
                conexion.Close();
                return false;
            }
        }
    }
}
