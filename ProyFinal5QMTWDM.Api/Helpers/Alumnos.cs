﻿using ProyFinal5QMTWDM.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ProyFinal5QMTWDM.Api.Helpers
{
    public class Alumnos
    {
        private readonly string _cadenaConexion = "";

        public Alumnos(string cadenaConexion)
        {
            _cadenaConexion = cadenaConexion;
        }

        public List<AlumnoGrupoCalificaciones> ObtenerAlumnosCalificaciones(int _idAlumno, ref string strErr)
        {
            DataTable dtRegistros = new DataTable();
            SqlDataAdapter sqlDataAdapter = null;
            SqlCommand sqlCommand = null;
            SqlConnection sqlConexion = new SqlConnection(_cadenaConexion);
            string sql = null;
            dtRegistros = new DataTable();
            try
            {
                strErr = "";

                sql = "SELECT A.[id],A.[idAlumno],U.[nombreCompleto],A.[idGrupo],G.[nombre] AS nombreGrupo,G.idProfesor	,M.nombreCompleto AS nombreProfesor, A.[calificacion],A.[fechaCreado] ";
                sql += "FROM [dbo].[Alumnos] A ";
                sql += "INNER JOIN[dbo].[Usuario] U ON(A.idAlumno = U.id) ";
                sql += "INNER JOIN[dbo].[Grupos] G ON(A.idGrupo = G.id)  ";
                sql += "INNER JOIN[dbo].[Usuario] M ON(G.idProfesor = M.id) ";
                sql += "WHERE A.[idAlumno] = " + _idAlumno + " ";


                sqlCommand = new SqlCommand(sql, sqlConexion);
                sqlCommand.CommandTimeout = 0;
                sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dtRegistros);


                if (dtRegistros.Rows.Count > 0)
                {
                    List<AlumnoGrupoCalificaciones> lista = new List<AlumnoGrupoCalificaciones>();
                    for (int i = 0; i < dtRegistros.Rows.Count; i++)
                    {
                        AlumnoGrupoCalificaciones alumno = new AlumnoGrupoCalificaciones();
                        alumno.Id = int.Parse(dtRegistros.Rows[i]["id"].ToString());
                        alumno.NombreGrupo = dtRegistros.Rows[i]["NombreGrupo"].ToString();
                        alumno.NombreProfesor = dtRegistros.Rows[i]["NombreProfesor"].ToString();
                        alumno.Calificacion = Convert.ToDecimal(dtRegistros.Rows[i]["Calificacion"].ToString());
                        lista.Add(alumno);
                    }
                    return lista;
                }
                else
                    return null;
            }
            catch (SqlException e)
            {
                sqlConexion.Close();
                strErr = e.Message.ToString() + " " + e.Source.ToString() + " " + e.StackTrace.ToString() + " " + e.TargetSite.Name.ToString();
                return null;
            }
        }

        public List<AlumnoUsuarioGrupoProfesor> ObtenerAlumnosUsuarioGrupo(int _idGrupo, ref string strErr)
        {
            DataTable dtRegistros = new DataTable();
            SqlDataAdapter sqlDataAdapter = null;
            SqlCommand sqlCommand = null;
            SqlConnection sqlConexion = new SqlConnection(_cadenaConexion);
            string sql = null;
            dtRegistros = new DataTable();
            try
            {
                strErr = "";

                if (_idGrupo == 0)
                {
                    sql = "SELECT U.[id] ,U.[id] AS[idAlumno]  ,U.[nombreCompleto] ,ISNULL(A.idGrupo,0) AS idGrupo,NULL AS nombreGrupo  ,ISNULL(G.idProfesor,0) AS idProfesor	  ,NULL as nombreProfesor	  ,ISNULL(A.[calificacion],0) AS calificacion	  ,ISNULL(A.fechaCreado,GETDATE()) AS fechaCreado ";
                    sql += "FROM [dbo].[Usuario] U ";
                    sql += "LEFT JOIN [dbo].[Alumnos] A ON(U.id = A.idAlumno) ";
                    sql += "LEFT JOIN [dbo].[Grupos] G ON(A.idGrupo = G.id) ";
                    sql += "WHERE A.idGrupo IS NULL AND U.tipo = 3 ";

                }
                else
                {
                    sql = "SELECT A.[id],A.[idAlumno],U.[nombreCompleto],A.[idGrupo],G.[nombre] AS nombreGrupo, G.idProfesor,M.nombreCompleto AS nombreProfesor,ISNULL(A.[calificacion],0) AS calificacion	,A.[fechaCreado] ";
                    sql += "  FROM[dbo].[Usuario] U ";
                    sql += " LEFT JOIN[dbo].[Alumnos] A ON(U.id = A.idAlumno) ";
                    sql += "  LEFT JOIN[dbo].[Grupos] G ON(A.idGrupo = G.id) ";
                    sql += "  INNER JOIN[dbo].[Usuario] M ON(G.idProfesor = M.id) ";
                    sql += "  WHERE A.idGrupo = " + _idGrupo + " AND U.tipo = 3";
                }

                //sql = "SELECT A.[id],A.[idAlumno],U.[nombreCompleto],A.[idGrupo],G.[nombre] AS nombreGrupo,G.idProfesor	,M.nombreCompleto AS nombreProfesor, A.[calificacion],A.[fechaCreado] ";
                //sql += "FROM [dbo].[Alumnos] A ";
                //sql += "INNER JOIN[dbo].[Usuario] U ON(A.idAlumno = U.id) ";
                //sql += "INNER JOIN[dbo].[Grupos] G ON(A.idGrupo = G.id)  ";
                //sql += "INNER JOIN[dbo].[Usuario] M ON(G.idProfesor = M.id) ";

                //if (_idGrupo != 0)
                //    sql += "WHERE A.[idGrupo] = " + _idGrupo + " AND U.[tipo] = 3";
                //else
                //    sql += "WHERE U.[tipo] = 3 ";

                //sql += "AND U.[tipo] = 3";


                sqlCommand = new SqlCommand(sql, sqlConexion);
                sqlCommand.CommandTimeout = 0;
                sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dtRegistros);


                if (dtRegistros.Rows.Count > 0)
                {
                    List<AlumnoUsuarioGrupoProfesor> lista = new List<AlumnoUsuarioGrupoProfesor>();
                    for (int i = 0; i < dtRegistros.Rows.Count; i++)
                    {
                        AlumnoUsuarioGrupoProfesor alumno = new AlumnoUsuarioGrupoProfesor();
                        alumno.Id = int.Parse(dtRegistros.Rows[i]["id"].ToString());
                        alumno.IdAlumno = int.Parse(dtRegistros.Rows[i]["IdAlumno"].ToString());
                        alumno.NombreCompleto = dtRegistros.Rows[i]["NombreCompleto"].ToString();
                        alumno.IdGrupo = int.Parse(dtRegistros.Rows[i]["IdGrupo"].ToString());
                        alumno.NombreGrupo = dtRegistros.Rows[i]["NombreGrupo"].ToString();
                        alumno.IdProfesor = int.Parse(dtRegistros.Rows[i]["IdProfesor"].ToString());
                        alumno.NombreProfesor = dtRegistros.Rows[i]["NombreProfesor"].ToString();
                        alumno.Calificacion = Convert.ToDecimal(dtRegistros.Rows[i]["Calificacion"].ToString());
                        alumno.FechaCreado = Convert.ToDateTime(dtRegistros.Rows[i]["FechaCreado"].ToString());
                        lista.Add(alumno);
                    }
                    return lista;
                }
                else
                    return null;
            }
            catch (SqlException e)
            {
                sqlConexion.Close();
                strErr = e.Message.ToString() + " " + e.Source.ToString() + " " + e.StackTrace.ToString() + " " + e.TargetSite.Name.ToString();
                return null;
            }
        }

        public bool InsertarAlumno(int _idAlumno, int _idGrupo, ref string strErr)
        {
            SqlConnection conexion = new SqlConnection(_cadenaConexion);
            conexion.Open();

            SqlTransaction sqlTransaccion = conexion.BeginTransaction(IsolationLevel.ReadCommitted);
            SqlCommand sqlCommand = conexion.CreateCommand();
            sqlCommand.Transaction = sqlTransaccion;

            try
            {
                sqlCommand.CommandText = "INSERT INTO [dbo].[Alumnos] ([idAlumno],[idGrupo],[calificacion],[fechaCreado]) VALUES (" + _idAlumno + "," + _idGrupo + ",0.0, GETDATE())";
                if (sqlCommand.ExecuteNonQuery() <= 0)
                    return false;
                sqlTransaccion.Commit();
                sqlTransaccion = null;
                conexion.Close();
                return true;
            }
            catch (Exception e)
            {
                if (sqlTransaccion != null)
                {
                    sqlTransaccion.Rollback();
                    sqlTransaccion = null;
                }
                strErr = e.Message.ToString() + " " + e.Source.ToString() + " " + e.StackTrace.ToString() + " " + e.TargetSite.Name.ToString();
                conexion.Close();
                return false;
            }
        }

        public bool ActualizarCalificacion(int _id, decimal _calificacion, ref string strErr)
        {

            SqlConnection conexion = new SqlConnection(_cadenaConexion);

            conexion.Open();

            SqlTransaction sqlTransaccion = conexion.BeginTransaction(IsolationLevel.ReadCommitted);
            SqlCommand sqlCommand = conexion.CreateCommand();
            sqlCommand.Transaction = sqlTransaccion;

            try
            {
                sqlCommand.CommandText = "UPDATE [dbo].[Alumnos] "
                              + "SET [calificacion] = " + _calificacion + " "
                              + "WHERE [id] = " + _id + " ";
                if (sqlCommand.ExecuteNonQuery() <= 0)
                    return false;
                sqlTransaccion.Commit();
                sqlTransaccion = null;
                conexion.Close();
                return true;
            }
            catch (Exception e)
            {
                if (sqlTransaccion != null)
                {
                    sqlTransaccion.Rollback();
                    sqlTransaccion = null;
                }
                strErr = e.Message.ToString() + " " + e.Source.ToString() + " " + e.StackTrace.ToString() + " " + e.TargetSite.Name.ToString();
                conexion.Close();
                return false;
            }
        }
    }
}
