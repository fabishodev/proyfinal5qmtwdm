﻿using ProyFinal5QMTWDM.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ProyFinal5QMTWDM.Api.Helpers
{
    public class Usuarios
    {
        private readonly string _cadenaConexion = "";

        public Usuarios(string cadenaConexion)
        {
            _cadenaConexion = cadenaConexion;
        }

        public List<Usuario> ObtenerUsuarios( ref string strErr)
        {
            DataTable dtRegistros = new DataTable();
            SqlDataAdapter sqlDataAdapter = null;
            SqlCommand sqlCommand = null;
            SqlConnection sqlConexion = new SqlConnection(_cadenaConexion);
            string sql = null;
            dtRegistros = new DataTable();
            try
            {
                strErr = "";

                sql = "SELECT [id] ,[nombreCompleto] ,[correo]   ,  [tipo]  ,[fecha]  FROM [dbo].[Usuario] ORDER BY [nombreCompleto] ASC";

                sqlCommand = new SqlCommand(sql, sqlConexion);
                sqlCommand.CommandTimeout = 0;
                sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dtRegistros);


                if (dtRegistros.Rows.Count > 0)
                {
                    List<Usuario> lista = new List<Usuario>();
                    for (int i = 0; i < dtRegistros.Rows.Count; i++)
                    {
                        Usuario usuario = new Usuario();
                        usuario.Id = int.Parse(dtRegistros.Rows[i]["id"].ToString());
                        usuario.Tipo = int.Parse(dtRegistros.Rows[i]["tipo"].ToString());
                        usuario.NombreCompleto = dtRegistros.Rows[i]["nombreCompleto"].ToString();
                        usuario.Correo = dtRegistros.Rows[i]["correo"].ToString();
                        usuario.Fecha = Convert.ToDateTime(dtRegistros.Rows[i]["Fecha"].ToString());
                        lista.Add(usuario);
                    }
                    return lista;
                }
                else
                    return null;
            }
            catch (SqlException e)
            {
                sqlConexion.Close();
                strErr = e.Message.ToString() + " " + e.Source.ToString() + " " + e.StackTrace.ToString() + " " + e.TargetSite.Name.ToString();
                return null;
            }
        }


        public Usuario ObtenerUsuario(string _correo, string _password, ref string strErr)
        {
            DataTable dtRegistros = new DataTable();
            SqlDataAdapter sqlDataAdapter = null;
            SqlCommand sqlCommand = null;
            SqlConnection sqlConexion = new SqlConnection(_cadenaConexion);
            string sql = null;
            dtRegistros = new DataTable();
            try
            {
                strErr = "";

                //sql = "SELECT [id],[nombreCompleto],[correo],[password],[tipo],[fecha] FROM [dbo].[Usuario] WHERE [correo] = '" + _correo + "' AND [password] = '" + _password + "' ";
                sql = "SELECT [id],[nombreCompleto],[correo],[password],[tipo],[fecha] FROM [dbo].[Usuario] WHERE [correo] = '" + _correo + "'";

                sqlCommand = new SqlCommand(sql, sqlConexion);
                sqlCommand.CommandTimeout = 0;
                sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                sqlDataAdapter.Fill(dtRegistros);


                if (dtRegistros.Rows.Count > 0)
                {
                    Usuario usuario = new Usuario();
                    usuario.Id = int.Parse(dtRegistros.Rows[0]["id"].ToString());
                    usuario.NombreCompleto = dtRegistros.Rows[0]["nombreCompleto"].ToString();
                    usuario.Correo = dtRegistros.Rows[0]["correo"].ToString();
                    usuario.Password = dtRegistros.Rows[0]["password"].ToString();
                    usuario.Tipo = int.Parse(dtRegistros.Rows[0]["tipo"].ToString());
                    usuario.Fecha = Convert.ToDateTime(dtRegistros.Rows[0]["fecha"].ToString());
                    return usuario;
                }
                else
                    return null;
            }
            catch (SqlException e)
            {
                sqlConexion.Close();
                strErr = e.Message.ToString() + " " + e.Source.ToString() + " " + e.StackTrace.ToString() + " " + e.TargetSite.Name.ToString();
                return null;
            }
        }
        public bool InsertarUsuario(string _correo, string _password, string _nombreCompleto, string _tipo, ref string strErr)
        {
            SqlConnection conexion = new SqlConnection(_cadenaConexion);
            conexion.Open();

            SqlTransaction sqlTransaccion = conexion.BeginTransaction(IsolationLevel.ReadCommitted);
            SqlCommand sqlCommand = conexion.CreateCommand();
            sqlCommand.Transaction = sqlTransaccion;

            try
            {
                sqlCommand.CommandText = "INSERT INTO [dbo].[Usuario] ([nombreCompleto],[correo],[password],[tipo],[fecha]) VALUES ('" + _nombreCompleto.ToUpper().Trim() + "','" + _correo.ToLower().Trim() + "','" + _password.Trim() + "'," + _tipo + ", GETDATE())";
                if (sqlCommand.ExecuteNonQuery() <= 0)
                    return false;
                sqlTransaccion.Commit();
                sqlTransaccion = null;
                conexion.Close();
                return true;
            }
            catch (Exception e)
            {
                if (sqlTransaccion != null)
                {
                    sqlTransaccion.Rollback();
                    sqlTransaccion = null;
                }
                strErr = e.Message.ToString() + " " + e.Source.ToString() + " " + e.StackTrace.ToString() + " " + e.TargetSite.Name.ToString();
                conexion.Close();
                return false;
            }
        }
    }
}
